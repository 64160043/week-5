package com.jidapa.week5;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    

    @Test
    public void shouldAdd1and1Is2(){
        int reasult = App.add(1, 1);
        assertEquals(2, reasult);
    }

    @Test
    public void shouldAdd2and1Is3(){
        int reasult = App.add(2, 1);
        assertEquals(3, reasult);

    }
    @Test
    public void shouldAddMin1and0IsMin1(){
        int reasult = App.add(-1, 0);
        assertEquals(-1, reasult);

    }

}
